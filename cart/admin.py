from django.contrib import admin
from .models import Cart

# Register your models here.


class scart(admin.ModelAdmin):
    list_display = ('__str__','user','total_price')

admin.site.register(Cart,scart)