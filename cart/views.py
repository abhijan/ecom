from django.shortcuts import render,HttpResponse
from django.shortcuts import redirect
from .models import Products,Cart


# Create your views here.


def add(request,slug):
    new_product = Products.objects.get(slug=slug)
    request.session.set_expiry(50)
    active = None

    try:
        active = request.session['cart']
    except:
        request.session['cart']= 'Empty'

    if request.session['cart'] != 'Empty' or active != None:
          cart = request.session['cart']
          update_cart = Cart.objects.get(id=cart)
          update_cart.products.add(new_product)
          update_cart.save()
          request.session['total_items'] = len(update_cart.products.all())

    else:
        new_cart = Cart()
        new_cart.save()
        new_cart.products.add(new_product)
        request.session['cart'] = new_cart.id
        request.session['total_items'] = len(new_cart.products.all())

    return redirect('/products/')

def view(request):
    try:
        cart_id = request.session['cart']
        cart_exists = Cart.objects.get(id=cart_id)
        message = 'Your Shopping Bag'
    except:
        message = 'Your Cart is empty'
        cart_exists = None
        try:
           request.session['total_items']==0
        except:
           pass

    return render(request,'cart/cart.html',{'cart_exists':cart_exists,'msg':message})

def delete(request):
    try:
        cart_id = request.session['cart']
        cart = Cart.objects.get(id=cart_id)
    except:
        request.session['total_items'] = 0
        msg = 'Your Cart is already Empty'
        return render(request, 'cart/cart.html', {'msg': msg})
    if cart:
        cart.delete()
        request.session['total_items'] = 0
        request.session['cart'] = None
        message = 'Your Cart is Empty'
        request.session.set_expiry(1)
        return render(request, 'cart/cart.html', {'msg': message})


