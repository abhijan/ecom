from django.db import models
import datetime

# Create your models here.


class ContactUs(models.Model):
    Name = models.CharField(max_length=50)
    Email = models.EmailField(max_length=300)
    Message = models.CharField(max_length=500)
    timestamp = models.DateTimeField(default=datetime.datetime.now())

    def __str__(self):
        return self.Name

    class Meta:
        ordering = ['-timestamp']