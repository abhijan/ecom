# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ContactUs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Name', models.CharField(max_length=50)),
                ('Email', models.EmailField(max_length=300)),
                ('Message', models.CharField(max_length=500)),
                ('timestamp', models.DateTimeField(default=datetime.datetime(2016, 6, 13, 19, 16, 35, 403768))),
            ],
            options={
                'ordering': ['-timestamp'],
            },
        ),
    ]
