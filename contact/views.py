from django.shortcuts import render
from .forms import ContactUsForm
from django.shortcuts import redirect
# Create your views here.


def contact(request):
    form = ContactUsForm(request.POST or None)
    if form.is_valid():
        this_form = form.save(commit=False)
        this_form.save()
        return render(request,'contact/form_submit.html',{'values':this_form})

    return render(request,'contact/form.html',{'form':form})



