import os
from django.conf.urls import include, url, patterns
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from  .settings import  BASE_DIR

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ecom.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    (r'[-\w+]/media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': os.path.join(BASE_DIR,'static/media')}),
    url(r'^accounts/',include('registration.backends.model_activation.urls')),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': os.path.join(BASE_DIR, "static")}),
    url(r'^$', 'products.views.home', name='home'),
    url(r'^pages/', include('django.contrib.flatpages.urls'),name= 'flat'),
    url(r'^products/$', 'products.views.products', name='Products'),
    url(r'^products/(?P<slug>[-\w]+)/$', 'products.views.product_single'),
    url(r'^products/(?P<slug>[-\w]+)/add/$', 'cart.views.add'),
    url(r'^contact/$', 'contact.views.contact', name='contact'),
    url(r'^cart/$', 'cart.views.view', name='cart'),
    url(r'^cartempty/$', 'cart.views.delete', name='empty'),
    #url(r'^submit/$', 'contact.views.form_submit', name='form_submit'),
)
