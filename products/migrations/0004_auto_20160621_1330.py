# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0003_auto_20160613_1916'),
    ]

    operations = [
        migrations.AddField(
            model_name='products',
            name='inventory',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='products',
            name='price',
            field=models.IntegerField(default=0.0),
        ),
        migrations.AddField(
            model_name='products',
            name='sku',
            field=models.CharField(default=b'abc', max_length=160),
        ),
    ]
