from django.shortcuts import render
from .models import Products


# Create your views here.
def home(request):

    return render(request,'home.html')

def products(request):
    products=Products.objects.all()
    return render(request,'results.html',{'products':products})


def product_single(request,slug):
    product=Products.objects.get(slug=slug)
    return render(request, 'product.html', {'product': product})